'use strict';

var loggerModel = require('./loggerModel');

 exports.init = function(app, config, rootLogger, loggerMap, loggerConfig) {

     if (loggerConfig !== undefined && loggerConfig.perfLogger !== undefined && loggerConfig.perfLogger !== null) {
       switch (loggerConfig.perfLogger) {
         case true:
           // adding basic performance logger
           var perfLogger = rootLogger.child({submodule: 'perfLogger'});
           // setting info as default level for perfLogger
           perfLogger.level(30);
           loggerMap['perfLogger'] = perfLogger;
           require('./basicPerfLogger')(app, config, perfLogger);
           break;
         case false:
           break;
         default:
           // adding basic performance logger
           var perfLogger = rootLogger.child({submodule: 'perfLogger'});
           // setting info as default level for perfLogger
           perfLogger.level(30);
           loggerMap['perfLogger'] = perfLogger;
           require(loggerConfig.perfLogger)(app, config, perfLogger);
       }

     }

     require('./hooks/remoteHook.js')(app, config);
     //require('./hooks/connectorHook.js')(app, config);

     var enableAPI = config.enableAPI || false;
     if(enableAPI) {
         // adding log management api
         loggerModel.addModel(app);
         app.once('started', function(){
             loggerModel.attachLoggerMap(loggerMap);
         });
     }
};
